import copy


def prune_level(level):
    level_width = len(level)
    for i in range(level_width - 1):
        duplicate_nodes_indices_set = set([])
        for j in range(i + 1, level_width):
            if level[i] == level[j]:
                duplicate_nodes_indices_set.add(i)
                duplicate_nodes_indices_set.add(j)
        duplicate_nodes_list = [level[i] for i in
            list(duplicate_nodes_indices_set)]
        if len(duplicate_nodes_list) > 1:
            merge_duplicate_nodes(duplicate_nodes_list)
    pruned_level = [node for node in level if not node.flagged_for_removal]
    return pruned_level


def merge_duplicate_nodes(duplicate_nodes_list):
    primary_node = duplicate_nodes_list[0]
    for node in duplicate_nodes_list[1:]:
        node.parent_node.add_child_node(primary_node)
        node.delete()
