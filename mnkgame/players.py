from typing import Tuple, Optional, List
from dataclasses import dataclass

import numpy as np

from mnkgame.game import to_string, EMPTY, get_matching_indices, is_winner, is_game_over


def human(board: np.ndarray, num_to_win: int, symbol: str, op_symbol: str) -> Tuple[int, int]:
    print(to_string(board))
    print(f'{num_to_win} to win')
    valid = False
    while not valid:
        row_index = int(input("Enter Row Index"))
        column_index = int(input("Enter Column Index"))
        valid = board[row_index, column_index] == EMPTY
    return row_index, column_index


def rand(board: np.ndarray, num_to_win: int, symbol: str, op_symbol: str) -> Tuple[int, int]:
    valid = False
    while not valid:
        n_rows, n_cols = board.shape
        row_index = np.random.randint(n_rows)
        column_index = np.random.randint(n_cols)
        valid = board[row_index, column_index] == EMPTY
    return row_index, column_index


def has_winning_move(board: np.ndarray, num_to_win: int, symbol: str) -> Optional[Tuple[int, int]]:
    possible_moves = get_matching_indices(board, EMPTY)
    for move in possible_moves:
        board = board.copy()
        board[move] = symbol
        if is_winner(board, num_to_win, symbol):
            return move
    return None


def one_step(board: np.ndarray, num_to_win: int, symbol: str, op_symbol: str) -> Tuple[int, int]:
    return (has_winning_move(board, num_to_win, symbol) or
            rand(board, num_to_win, symbol, op_symbol))


def one_step_with_blocking(board: np.ndarray, num_to_win: int, symbol: str, op_symbol: str) -> Tuple[int, int]:
    return (has_winning_move(board, num_to_win, symbol) or
            has_winning_move(board, num_to_win, op_symbol) or
            rand(board, num_to_win, symbol, op_symbol))


@dataclass
class GameTreeNode:
    board: np.ndarray
    is_over: bool
    winning_symbol: Optional[str]
    child_nodes: Optional[list] = None


def get_possible_boards(board: np.ndarray, symbol: str) -> List[np.ndarray]:
    possible_moves = get_matching_indices(board, EMPTY)
    boards = []
    for move in possible_moves:
        board = board.copy()
        board[move] = symbol
        boards.append(board)
    return boards


def make_game_tree(board: np.ndarray, num_to_win: int, symbols: List[str], current_symbol: str) -> GameTreeNode:
    is_over, winning_symbol = is_game_over(board, symbols, num_to_win)
    node = GameTreeNode(board=board, is_over=is_over, winning_symbol=winning_symbol)
    if not is_over:
        boards = get_possible_boards(board, current_symbol)
        child_symbol = symbols[symbols.index(current_symbol) + 1 % len(symbols)]
        node.child_nodes = [make_game_tree(board, num_to_win, symbols, child_symbol) for board in boards]
    return node

