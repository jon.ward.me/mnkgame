from typing import List, Tuple, Callable, Optional

import numpy as np

EMPTY = '-'


def printv(str_, verbose):
    if verbose:
        print(str_)


def make_board(n_rows: int, n_cols: int) -> np.ndarray:
    return np.full((n_rows, n_cols), fill_value=EMPTY)


def to_string(board: np.ndarray) -> str:
    return '\n'.join([''.join(row) for row in board])


def get_matching_indices(board: np.ndarray, symbol: str) -> List[Tuple[int, int]]:
    it = np.nditer(board, flags=['multi_index'])
    matching_indices = []
    while not it.finished:
        if it[0] == symbol:
            matching_indices.append(it.multi_index)
        it.iternext()
    return matching_indices


def is_board_full(board: np.ndarray):
    return bool(get_matching_indices(board, symbol=EMPTY))


def get_transposes(idx, num_to_win) -> List[List[Tuple[int, int]]]:
    transposes = []
    for offset in range(0, -num_to_win, -1):
        horizontal = [(idx[0] + offset + shift, idx[1]) for shift in range(1, num_to_win)]
        vertical = [(idx[0], idx[1] + offset + shift) for shift in range(1, num_to_win)]
        up_diagonal = [(idx[0] + offset + shift, idx[1] + offset + shift) for shift in range(1, num_to_win)]
        down_diagonal = [(idx[0] + offset + shift, idx[1] - offset - shift) for shift in range(1, num_to_win)]
        transposes.extend([horizontal, vertical, up_diagonal, down_diagonal])
    return transposes


def is_winner(board: np.ndarray, num_to_win: int, symbol: str):
    indices = get_matching_indices(board, symbol)
    for idx in indices:
        transposes = get_transposes(idx, num_to_win)
        if all(transpose in indices for transpose in transposes):
            return True
    return False


def play(num_to_win: int, board: np.ndarray,
         players: Tuple[Callable[[np.ndarray, int, str], Tuple[int, int]],
                        Callable[[np.ndarray, int, str], Tuple[int, int]]],
         symbols: Tuple[str, str], verbose: bool = True):
    printv("Lets Play Tic-Tac-Toe!", verbose)
    printv(f"First Player to {num_to_win} in a row wins!", verbose)
    move_history = []
    while not is_game_over(board, symbols, num_to_win):
        for player, symbol in zip(players, symbols):
            move = player(board, num_to_win, symbol)
            board[move] = symbol
            printv(to_string(board), verbose)
            move_history.append(move)
    return move_history, board


def is_game_over(board: np.ndarray, symbols: List[str], num_to_win: int) -> Tuple[bool, Optional[str]]:
    if is_board_full(board):
        return True, EMPTY
    for symbol in symbols:
        if is_winner(symbol, num_to_win, board):
            return True, symbol
    return False, None


if __name__ == "__main__":
    print(get_transposes((0, 1), 3))
    board_ = make_board(2, 3)
    print(to_string(board_))
    symbols_ = ['x', 'o']
    # final_move_history, final_board = play(num_to_win=3, symbols=symbols)
