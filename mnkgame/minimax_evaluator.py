

def compute_terminal_score(winner_index, num_players):
    if winner_index == -1:
        scores = [0 for player_num in range(self.mnkgame.num_players)]
    else:
        scores = [-10 for player_num in range(self.mnkgame.num_players)]
        scores[winner_index] = 10
    return scores

def compute_minimax_scores(scores, child_scores_list):
    worst_child_scores = []
    for child_scores in child_scores_list:
        other_players_scores = (child_scores[:self.player_index] +
                                child_scores[self.player_index + 1:])
        best_other_player_score = max(other_players_scores)
        worst_own_score = -best_other_player_score
        worst_child_scores.append(worst_own_score)
    minimax_score = min(worst_child_scores)
    for i in range(len(scores)):
        if i == self.player_index:
            scores[i] = minimax_score
        else:
            scores[i] = min(-minimax_score, scores[i])
    return scores
