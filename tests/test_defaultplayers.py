from mnkgame.mnkgame.mnkplayers import defaultplayers


def test_types():
    assert type(defaultplayers.Human) == dict
    assert type(defaultplayers.RandomBot) == dict
    assert type(defaultplayers.OneStepBot) == dict
    assert type(defaultplayers.OneStepBlockingBot) == dict
    assert type(defaultplayers.MinimaxBot) == dict
